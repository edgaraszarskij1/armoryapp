import React from 'react'
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#8b0000',
    },
    secondary: {
      main: '#ac0404',
    },
    mainBack: {
        backgroundColor: '#364150'
    }
  },
  typography: {
    useNextVariants: true,
  }
});

export default function ThemeColor(props) {

    const { children } = props
    return (
      
      <div> 
        <MuiThemeProvider theme={theme} >
          <main >
            {children}
          </main>
        </MuiThemeProvider>
      </div>
      
    );
}

