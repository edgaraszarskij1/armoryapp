import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
      alignItems: 'center',
      display: 'flex',
      '& > * + *': {
        marginLeft: theme.spacing(4),
      },
    },
  }));

export default function CircularUnderLoad() {

    const classes = useStyles();
  return <div className={classes.root} ><CircularProgress size = {200} /></div>;

}