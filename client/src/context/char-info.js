import React from 'react'

const characterInfo = React.createContext({
    newRealm: null,
    newName: null,
    searchFor: () => {}
});

export default characterInfo;