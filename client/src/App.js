import React from 'react';
import { Route } from 'react-router-dom';
import AppBar from './components/AppBar';
import Card from './components/Card';
import Armory from './components/Armory';
import Theme from './containers/Theme'
import CssBaseline from '@material-ui/core/CssBaseline'

function App() {
  return (
    <React.Fragment >
      <Theme>
        <AppBar>     
            <Route path='/' exact component={Armory} />
            <Route path='/character/:realm/:name' component={Card} />
        </AppBar>
      </Theme>
    </React.Fragment>
  );
}

export default App;
