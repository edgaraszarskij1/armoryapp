
export default class apiCalls {
  getCommunity = (action, kind, target) => {
    return fetch(`http://localhost:3001/profile/wow/${action}/${kind}/${target}?namespace=profile-eu&locale=en_EU`, {
    }).then(resp => {
      return Promise.resolve(resp.json());
    })
  }

  getGameData = (action, kind, target) => {
    return fetch(`http://localhost:3001/data/wow/${action}/${kind}/${target}`, {
    }).then(resp => {
      return Promise.resolve(resp.json());
    })
  }

  getProfile = (action, kind, target) => {
    return fetch(`http://localhost:3001/profile/wow/${action}/${kind}/${target}`, {
    }).then(resp => {
      return Promise.resolve(resp.json());
    })
  }

  getCharacterInfo = (role, id) => {
    return fetch(`http://localhost:3001/data/wow/playable-${role}/${id}?namespace=static-eu&locale=en_US`, {
    }).then(resp => {
      return Promise.resolve(resp.json());
    })
  }

  getRealmInfo = () => {
    return fetch('http://localhost:3001/data/wow/realm/index?namespace=dynamic-eu&locale=en_US', {
    }).then(resp => {
      return Promise.resolve(resp.json());
    })
  }

  getPicture = (action,kind,target) => {
    return fetch(`http://localhost:3001/profile/wow/${action}/${kind}/${target}/character-media?namespace=profile-eu&locale=en_EU`, {
    }).then(resp => {
      return Promise.resolve(resp.json());
    })
  }

  getHallOfFame = (raid,faction) => {
    return fetch(`http://localhost:3001/data/wow/leaderboard/hall-of-fame/${raid}/${faction}?namespace=dynamic-eu&locale=en_EU`,{
    }).then(resp => {
      return Promise.resolve(resp.json());
    })
  }

  getRealmBestRun = (dungeonID) => {
    
    let requests = dungeonID.map(dungeonID => fetch(`http://localhost:3001/data/wow/connected-realm/1305/mythic-leaderboard/${dungeonID.id}/period/743?namespace=dynamic-eu&locale=en_US`));
    return Promise.all(requests)
    .then(responses => Promise.all(responses.map(resp => resp.json())))
  }

  getDugeonsId = () => {
    return fetch(`http://localhost:3001/data/wow/mythic-keystone/dungeon/index?namespace=dynamic-eu&locale=en_US`,{
    }).then(resp =>{
      return Promise.resolve(resp.json())
    })
  }

  getSingleRealmBestRun = (dungeonID) => {
    return fetch(`http://localhost:3001/data/wow/connected-realm/1305/mythic-leaderboard/${dungeonID}/period/743?namespace=dynamic-eu&locale=en_US`,{
    }).then(resp => {
      return Promise.resolve(resp.json());
    })
  }

  getAnything = (fullRoute) => {
    return fetch(`http://localhost:3001${fullRoute}`,{
    }).then(resp => {
      return Promise.resolve(resp.json());
    })
  }

}



//'https://eu.api.blizzard.com/data/wow/playable-race/2?namespace=static-eu&locale=en_EU'
///profile/wow/character/kazzak/kanekí/character-media?namespace=profile-eu&locale=en_EU
// http://localhost:3001/data/wow/leaderboard/hall-of-fame/uldir/alliance?namespace=dynamic-eu&locale=en_US&