import React, { useState, useEffect } from 'react';
import Calls from '../apiCalls';
import TopRealmRuns from '../components/TopRealmRuns';

export default function GetTopMythic(props) {

  const apiCalls = new Calls();
  const [id, setID] = useState([]);
  const [realmBest, setRealmBest] = useState([]);

  useEffect(() => {
    apiCalls.getDugeonsId()
      .then(data => { setID(data.dungeons)})

  }, [])

  useEffect(() => {
    if(id.length !==0){
      apiCalls.getRealmBestRun(id).then(data => {setRealmBest(data)}) 
    }
  }, [id])


  return <React.Fragment>{realmBest !== null ? <TopRealmRuns realmBest = { realmBest } /> : <div>Error</div> }</React.Fragment>

}

//'1305' '743'