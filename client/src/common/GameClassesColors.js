 const  getClassColor = (classSpecID) => {
    switch (classSpecID) {
      case 268: {
        return 'monk'
      }
      case 269: {
        return 'monk'
      }
      case 270: {
        return 'monk'
      }
      case 62: {
        return 'mage'
      }
      case 64: {
        return 'mage'
      }
      case 63: {
        return 'mage'
      }
      case 256: {
        return 'priest'
      }
      case 257: {
        return 'priest'
      }
      case 258: {
        return 'priest'
      }
      case 102: {
        return 'druid'
      }
      case 103: {
        return 'druid'
      }
      case 104: {
        return 'druid'
      }
      case 105: {
        return 'druid'
      }
      case 65: {
        return 'paladin'
      }
      case 66: {
        return 'paladin'
      }
      case 70: {
        return 'paladin'
      }
      case 71: {
        return 'warrior'
      }
      case 72: {
        return 'warrior'
      }
      case 73: {
        return 'warrior'
      }
      case 250: {
        return 'death_knight'
      }
      case 251: {
        return 'death_knight'
      }
      case 252: {
        return 'death_knight'
      }
      case 265: {
        return 'warlock'
      }
      case 266: {
        return 'warlock'
      }
      case 267: {
        return 'warlock'
      }
      case 577: {
        return 'demonHunter'
      }
      case 581: {
        return 'demonHunter'
      }
      case 253: {
        return 'hunter'
      }
      case 254: {
        return 'hunter'
      }
      case 255: {
        return 'hunter'
      }
      case 71: {
        return 'warrior'
      }
      case 72: {
        return 'warrior'
      }
      case 73: {
        return 'warrior'
      }
      case 259: {
        return 'rogue'
      }
      case 260: {
        return 'rogue'
      }
      case 261: {
        return 'rogue'
      }
      case 262: {
        return 'shaman'
      }
      case 263: {
        return 'shaman'
      }
      case 264: {
        return 'shaman'
      }
    }
  }

  export default getClassColor;