import tank from '../Tank.png';
import dps from '../Damage.png';
import healer from '../Healer.png';


const  roleIcons = (classSpecID) => {
    switch (classSpecID) {
      case 268: {
        return tank
      }
      case 269: {
        return dps
      }
      case 270: {
        return healer
      }
      case 62: {
        return dps
      }
      case 64: {
        return dps
      }
      case 63: {
        return dps
      }
      case 256: {
        return healer
      }
      case 257: {
        return healer
      }
      case 258: {
        return dps
      }
      case 102: {
        return dps
      }
      case 103: {
        return dps
      }
      case 104: {
        return tank
      }
      case 105: {
        return healer
      }
      case 65: {
        return healer
      }
      case 66: {
        return tank
      }
      case 70: {
        return dps
      }
      case 71: {
        return dps
      }
      case 72: {
        return dps
      }
      case 73: {
        return tank
      }
      case 250: {
        return tank
      }
      case 251: {
        return dps
      }
      case 252: {
        return dps
      }
      case 265: {
        return dps
      }
      case 266: {
        return dps
      }
      case 267: {
        return dps
      }
      case 577: {
        return dps
      }
      case 581: {
        return tank
      }
      case 253: {
        return dps
      }
      case 254: {
        return dps
      }
      case 255: {
        return dps
      }
      case 259: {
        return dps
      }
      case 260: {
        return dps
      }
      case 261: {
        return dps
      }
      case 262: {
        return dps
      }
      case 263: {
        return dps
      }
      case 264: {
        return healer
      }
    }
  }

  export default roleIcons;