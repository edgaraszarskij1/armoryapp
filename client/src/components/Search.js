import React, { useState, useEffect } from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Grid from '@material-ui/core/Grid';
import SearchIcon from '@material-ui/icons/Search';
import { withRouter } from 'react-router-dom'
import Calls from '../apiCalls';

const useStyles = makeStyles(theme => ({
  text: {
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
  },
  input: {
    height: 5,
    color: "white",
    "&$floatingLabelFocusStyle": {
      color: "white"
    }
  },
  floatingLabelFocusStyle: {},
  input2: {
    height: 17,
    '&:-webkit-autofill': {
      transitionDelay: '9999s',
      transitionProperty: 'background-color, color',

    },
    color: "white",
    "&$floatingLabelFocusStyle": {
      color: "white"
    }
  },
  size: {
    width:35,
    height:35
  },
  onHoverSearchIcon: {
    textDecoration: 'none',
    '&:hover': {
      cursor: 'pointer'
    }
  },
}))

function Search(props) {
  let apiCalls = new Calls()
  const classes = useStyles();
  const [realms, setRealms] = useState([])
  const [name, setName] = React.useState('');
  const [realm, setRealm] = React.useState('');

  const handleChange = event => {
    setName(event.target.value);
  };

  const handleChanges = event => {
    setRealm(event.target.value);
  };

  const handleClickSearch = (realm, name) => {
    props.history.push({
      pathname: '/character/' + realm.toLocaleLowerCase() + '/' + name.toLocaleLowerCase()
    })
  }

  useEffect(() => {
    apiCalls.getRealmInfo().then(data => setRealms(data.realms))
  }, [])

  return (
    <form >
      <Grid container spacing={1}>
        <Grid item >
          <Autocomplete
            id="combo-box-demo"
            options={realms}
            size='small'
            getOptionLabel={option => option.name}
            style={{ width: 300 }}
            renderInput={params => <TextField {...params} label="Realm" variant="outlined"
              value={realm}
              onSelect={handleChanges}
              inputProps={{ ...params.inputProps, className: classes.input }}
              className={classes.text}
              InputLabelProps={{
                classes: {
                  root: classes.input,
                  focused: classes.floatingLabelFocusStyle,
                }
              }} />}
          />
        </Grid>
        <Grid item >
          <TextField id="outlined-basic" variant="outlined" size='small' style={{ width: 300 }}
            label="Name"
            className={classes.text}
            value={name}
            onChange={handleChange}
            inputProps={{ className: classes.input2 }}
            InputLabelProps={{
              classes: {
                root: classes.input,
                focused: classes.floatingLabelFocusStyle,
              }
            }}
          />
        </Grid>
        <Grid item>
          <SearchIcon 
          onClick={()=>{handleClickSearch(realm,name)}} 
          style={{marginTop:3,marginLeft:-8}} 
          className={[classes.size, classes.onHoverSearchIcon].join(' ')}/>
        </Grid>
      </Grid>
    </form>
  );
}

export default withRouter(Search)