import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { ThemeProvider } from '@material-ui/core'
import { createMuiTheme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const theme = createMuiTheme({
  overrides: {
    MuiTableCell: {
      root: {
        padding: '0px 0px',
      },
      alignCenter: {
        paddingRight: 34
      }
    },
    MuiTableRow: {
      hover: {
        "&$hover:hover": {
          backgroundColor: '#C0C0C0',
        },
      },
    },
  },
});

const useStyles = makeStyles({
  table: {
    width: '64%'
  },
});

export default function SimpleTable(props) {
  const classes = useStyles();

  const armoryTable = () => {
    return (
      <ThemeProvider theme={theme}>
        <TableContainer component={Paper}>
          <Box fontWeight={600} fontSize={16} style={{ backgroundColor: '#808080', width: "100%", padding: 8, textAlign: 'center' }}>Hall of Fame </Box>
          <Table aria-label="simple table" style={{ background: '#DCDCDC' }}>
            <TableBody>
              {props.guildsArray.slice(0, 5).map(data => (
                <TableRow key={data.timestamp} hover>
                  {data.faction.type === 'HORDE' ?
                    <TableCell component="th" style={{ color: '#8C1616' }} scope="data">
                      <Box fontWeight={600} fontSize={13} m={1}>
                        {data.guild.name}
                      </Box>
                    </TableCell>
                    : <TableCell component="th" style={{ color: '#162c57' }} scope="data">
                      <Box fontWeight={600} fontSize={13} m={1}>
                        {data.guild.name}
                      </Box></TableCell>
                  }
                  <TableCell align="center">
                    <Box fontWeight={500} fontSize={13} m={1}>
                      {data.guild.realm.slug.charAt(0).toUpperCase() + data.guild.realm.slug.slice(1) +
                        '(' + data.region.toUpperCase() + ')'}</Box>
                  </TableCell>
                  <TableCell align="right">
                    <Box fontWeight={500} fontSize={13} m={1}>
                      {data.faction.type.slice(0, 1) + data.faction.type.slice(1).toLowerCase()}
                    </Box>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </ThemeProvider>
    )
  }

  return (
    armoryTable()
  );
}