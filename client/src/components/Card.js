import React, { useState, useEffect, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Calls from '../apiCalls';
import Skeleton from '@material-ui/lab/Skeleton'
import CharContext from '../context/char-info'
 
const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

export default function MediaCard(props) {
  
  const classes = useStyles();
  const [picture, setPicture] = useState('');
  const [name, setName] = useState([]);
  const [characterClass, setCharacterClass] = useState([]);
  const [race, setRace] = useState([]);
  const [level, setLevel] = useState([]);
  const [newRace, setNewRace] = useState([])
  const [playableClass, setPlayableClass] = useState([])

  let apiCalls = new Calls()
  
    apiCalls.getCommunity('character', props.match.params.realm, props.match.params.name)
      .then(data => (
        setRace(data.race.id), setName(data.name),
        setCharacterClass(data.character_class.id), setLevel(data.level)));
    apiCalls.getPicture('character', props.match.params.realm, props.match.params.name).then(data => (setPicture(data.bust_url)))
  
  if (race.length !== 0 && characterClass.length !== 0) {
    apiCalls.getCharacterInfo('race', race).then(classInfo => setNewRace(classInfo.name))
    apiCalls.getCharacterInfo('class', characterClass).then(classInfo => setPlayableClass(classInfo.name))
  }
 
  return (
    picture == '' ? <Skeleton variant="rect" width={210} height={118} /> : <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={picture}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
          <Typography>{newRace} {playableClass} {level}</Typography>
          <Typography variant="body2" color="textSecondary" component="p">
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
