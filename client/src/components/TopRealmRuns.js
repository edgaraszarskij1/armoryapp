import React, { useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { ThemeProvider } from '@material-ui/core'
import { createMuiTheme, makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Spinner from '../containers/Spinner'
import roleIcons from '../common/RoleIcons'
import getClassColor from '../common/GameClassesColors'
import './gameClassColors.css';

const theme = createMuiTheme({
  overrides: {
    MuiTableCell: {
      root: {
        padding: '0px 0px',
      },
      alignCenter: {
        paddingRight: 34
      }
    },
    MuiTableRow: {
      hover: {
        "&$hover:hover": {
          backgroundColor: '#C0C0C0',
        },
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({
  hoverIcon: {
    textDecoration: 'none',
    '&:hover': {
      cursor: 'pointer',
      fontSize: 16
    }
  },
}))

function TopRealmRuns(props) {

  const classes = useStyles();

  const clickHandler = (realm, name) => {
    props.history.push({
      pathname: '/character/' + realm + '/' + name.toLowerCase()
    })
  }
  useEffect(() => {

  }, [])
  const createTable = () => {
    console.log(props.realmBest)
    return (<React.Fragment>
      {props.realmBest.length !== 0 ? <ThemeProvider theme={theme}>
        <TableContainer component={Paper} >
          <Box fontWeight={600} fontSize={16}
            style={{
              backgroundColor: '#808080',
              width: "100%",
              padding: 8,
              extAlign: 'center'
            }}>Highest key done (Kazzak) </Box>
          <Table aria-label="simple table" style={{ background: '#DCDCDC' }}>
            <TableBody>
              {props.realmBest.map((run) => (<TableRow key={run.name} hover >
                <TableCell component="th" style={{ margin: '0' }} scope="data" >
                  <Box fontWeight={600} fontSize={13} style={{ margin: 4 }} m={1}>
                    {run.name + ' ' + `(+${run.leading_groups[0].keystone_level})`}
                  </Box>
                </TableCell>
                {run.leading_groups[0].members.map(member => (
                  <TableCell key={member.profile.name} align="left">
                    <Box fontWeight={500} fontSize={13} m={1}
                      onClick={() => clickHandler(member.profile.realm.slug, member.profile.name)}
                      className={[getClassColor(member.specialization.id), classes.hoverIcon].join(' ')}>
                      <img src={roleIcons(member.specialization.id)} style={{ width: 18, pading: 2, marginRight: 2 }} />
                      {member.profile.name}
                    </Box>
                  </TableCell>
                ))}
              </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </ThemeProvider>
        : <Spinner />}
    </React.Fragment>
    )
  }

  return (
    createTable()
  )

}

export default withRouter(TopRealmRuns)