import React, { useState, useEffect } from 'react';
import Calls from '../apiCalls';
import ArmoryTable from './ArmoryTable'
import Spinner from '../containers/Spinner'
import GetTopMythic from '../common/GetRealmBest';
import Grid from '@material-ui/core/Grid';

export default function Armory(props) {
  const apiCalls = new Calls();

  const [hordeGuild, setHordeGuild] = useState([])
  const [allianceGuild, setAllianceGuild] = useState([])
  let sortedGuilds = []

  useEffect(() => {
    apiCalls.getHallOfFame('uldir', 'horde')
      .then(data => { setHordeGuild(data.entries) })
    apiCalls.getHallOfFame('uldir', 'alliance')
      .then(data => { setAllianceGuild(data.entries) })

  }, [])


  if (hordeGuild.length !== 0 && allianceGuild.length !== 0) {

    let generalGuildProgress = [];
    generalGuildProgress = hordeGuild.concat(allianceGuild)
    generalGuildProgress.sort((a, b) => (a.timestamp > b.timestamp) ? 1 : ((b.timestamp > a.timestamp) ? -1 : 0));
    if (generalGuildProgress.length !== 0) {
      sortedGuilds = generalGuildProgress
    }
  }
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          {sortedGuilds.length !== 0 ? <ArmoryTable guildsArray={sortedGuilds} /> : <Spinner />}
        </Grid>
        <Grid item xs>
          <GetTopMythic />
        </Grid>
      </Grid>
    </div>
  )
}
