var BnetStrategy = require("passport-bnet")
var express = require('express')
var passport = require('passport');
var app = express();
var cookieParser = require('cookie-parser');
var session = require('express-session')
require('dotenv').config();
var cors = require('cors')
var OAuth2RefreshTokenStrategy = require('passport-oauth2-middleware').Strategy
app.use(cors())
const { createProxyMiddleware } = require('http-proxy-middleware')
const fetch = require('node-fetch')
let user = {};
const port = 3001
var BNET_ID = process.env.BNET_ID;
var BNET_SECRET = process.env.BNET_SECRET;

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (obj, done) {
  done(null, obj);
});

var refreshStrategy = new OAuth2RefreshTokenStrategy({
  refreshWindow: 10, // Time in seconds to perform a token refresh before it expires
  userProperty: 'ticket', // Active user property name to store OAuth tokens
  authenticationURL: '/login', // URL to redirect unathorized users to
  callbackParameter: 'callback' //URL query parameter name to pass a return URL
});

passport.use('main', refreshStrategy)

passport.use(new BnetStrategy({
  clientID: BNET_ID,
  clientSecret: BNET_SECRET,
  callbackURL: "http://localhost:3001/auth/bnet/callback",
  authorizationURL: 'https://eu.battle.net/oauth/authorize',
  passReqToCallback: false
},
  (accessToken, refreshToken, profile, cb) => {
    user = { ...profile };
    return cb(null, profile);
  }, refreshStrategy.getOAuth2StrategyCallback())
)
refreshStrategy.useOAuth2Strategy(BnetStrategy)

app.use(cookieParser());
app.use(session({
  secret: 'blizzard',
  saveUninitialized: true,
  resave: true
}));

app.use(passport.initialize());
app.use(passport.session())

app.get('/auth/bnet', passport.authenticate('bnet', {
  scope: ['wow.profile']
}));

app.get('/auth/bnet/callback',
  passport.authenticate('bnet', { failureRedirect: '/' }),
  (req, res) => {
    res.redirect("/user");
  })

app.get("/user", (req, res) => {
  res.send(user);
});

app.get("/logout", (req, res) => {
  console.log("logging out!");
  user = {};
  res.redirect("/");
});

const wow = createProxyMiddleware({
  target: 'https://eu.api.blizzard.com',
  changeOrigin: true, // for vhosted sites, changes host header to match to target's host
  logLevel: 'debug',
  onProxyReq
})
app.use('/', wow)
function onProxyReq(proxyReq, req, res) {
  proxyReq.setHeader('Authorization', 'Bearer ' + user.token);
}
app.listen(port)